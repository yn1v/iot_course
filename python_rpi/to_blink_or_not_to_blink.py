# Neville A. Cross
# https://gitlab.com/yn1v/iot_course/tree/master/python_rpi
# June 2nd, 2019

# This code implement the current library for GPIO
# https://gpiozero.readthedocs.io

from gpiozero import LED, Button
from signal import pause

button = Button(20)
led = LED(21)

led.blink()
button.when_pressed = led.on
button.when_released = led.blink

pause()
