# Neville A. Cross
# June 16th, 2019.
#
# https://gitlab.com/yn1v/iot_course/blob/master/python_rpi/led_fading.py
#
# Code example from
# https://sourceforge.net/p/raspberry-gpio-python/wiki/PWM/
#

import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)

p = GPIO.PWM(12, 50)  # channel=12 frequency=50Hz
p.start(0)
try:
    while 1:
        for dc in range(0, 101, 5):
            # iterable dc go from 0 to 100 at 5 increments
            p.ChangeDutyCycle(dc)
            # set dutyclycle to the currente dc iterable value
            time.sleep(0.1)
            # wait 1/10 second = 2 seconds from 0 to 100
        for dc in range(100, -1, -5):
            # go from 100 to 1 at 5 increments
            p.ChangeDutyCycle(dc)
            # set dutyclycle to the currente dc iterable value
            time.sleep(0.1)
            # wait 1/10 second = 2 seconds from 100 to 0
except KeyboardInterrupt:
    pass
p.stop()
GPIO.cleanup()
