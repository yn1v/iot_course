# Neville A. Cross
# June 16th, 2019
#
# https://gitlab.com/yn1v/iot_course/blob/master/python_rpi/web_request.py
#
import http.client                             # import library
site = "www.uci.edu"                           # web site address
conn = http.client.HTTPSConnection(site)       # create object
conn.request("GET", "/")                       # define request
data = conn.getresponse()                      # get data
# print(data.status, data.reason)              # debug connection
raw_page = data.read()                         # store data
conn.close()                                   # close connection
page = raw_page.decode()                       # decode byte array into string
lines = []                                     # create a list to store lines
last_point = 0                                 # variable for storeing end line
for i in range(len(page)):                     # interate along the string
    if page[i] == "\n":                        # find end line
        a = page[last_point:i+1]               # create a slice 
        lines.append(a)                        # add slice to list
        last_point = i+1                       # move end line possition
    else:                                      # if not an end line
        pass                                   # move to next character
for i in range(3):                             # look for the first 3 lines
    print("Line", i+1, "is:", end="")          # line numering
    print(lines[i].rstrip())                   # print line without the end line

