# Neville A. Cross
# https://gitlab.com/yn1v/iot_course/tree/master/python_rpi
# June 2nd, 2019

# This is the script using the old GPIO library
# Most code come form SparkFun site
# https://learn.sparkfun.com/tutorials/raspberry-gpio

import RPi.GPIO as GPIO
from time import sleep

button = 38
led = 40

GPIO.setmode(GPIO.BOARD)
GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(led, GPIO.OUT)

while True:
    if GPIO.input(button): # Button HIGH == Open
        GPIO.output(led, GPIO.LOW)
        sleep(0.5)
        GPIO.output(led, GPIO.HIGH)
        sleep(0.5)
        print("Pin", button, "is HIGH")
    else:  # Button LOW == Close (drain to ground)
        GPIO.output(led, GPIO.HIGH) 
        print("Pin", button, "is LOW")
