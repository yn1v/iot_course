# Neville A. Cross
# https://gitlab.com/yn1v/iot_course/tree/master/python_rpi
# June 2nd, 2019

# This code implement the current library for GPIO
# https://gpiozero.readthedocs.io

from gpiozero import LED, Button
from signal import pause

button = Button(20)
led = LED(21)

# led.blink()
# button.when_pressed = led.on
# button.when_released = led.blink

def pushed():
    led.on()
    print("Button is pressed")

def released():
    led.blink(on_time=0.5, off_time=0.5)
    print("Button is released")
    
led.blink(on_time=0.5, off_time=0.5)
button.when_pressed = pushed
button.when_released = released

pause()
