# Neville A. Cross
# June 6th, 2019
# 
# https://gitlab.com/yn1v/iot_course/tree/master/server
#

import socket

host = "192.168.10.14"
port = 1234

srv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
srv_socket.bind((host,port ))
srv_socket.listen(5)
while True:
    c, addr = srv_socket.accept()
    print('"Got a request" from IP:', addr)
    c.close()
