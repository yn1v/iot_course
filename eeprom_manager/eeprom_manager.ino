#include <EEPROM.h>

String inputString, command, dataAddress, dataValue;
int addressNumber, valueNumber;

void setup() {
  Serial.begin(9600);
  Serial.println("Use 'write' or 'read'. Write takes two arguments, an EEPROM address and Value to Write");
  Serial.println("Read takes the EEPROM address.");
  Serial.println("Example: 'write 12 24'");
  Serial.println("Example: 'read 12'");
}

void loop() {
  if (Serial.available() > 0) {
     int i = 0;
     command = "";
     dataAddress = "";
     dataValue = "";
     addressNumber = -1;
     valueNumber = -1;
     
     inputString = Serial.readString();  
    
     while (isWhitespace(inputString[i]) && (i < inputString.length())) { 
           i++; // Find the first character that is not a blank space
           }
    
     while (isAlpha(inputString[i]) && (i < inputString.length())) { 
           command += inputString[i];  //Get the characters for the command
           i++;
           }

    
     while (isWhitespace(inputString[i]) && (i < inputString.length())) { 
           i++; // Move along the blank spaces
           }
    
     while (isDigit(inputString[i]) && (i < inputString.length())) {
           dataAddress += inputString[i]; // Get the numbers for the address
           i++;
           }
    
     while (isWhitespace(inputString[i]) && (i < inputString.length())) {
            i++; // Move along blank spaces
           }

      while (isDigit(inputString[i]) && (i < inputString.length())) {
             dataValue += inputString[i]; // Get the number to write
             i++;
             }

    addressNumber = dataAddress.toInt();  // Convert string to integers
    valueNumber = dataValue.toInt();
 
    if ((addressNumber < 0) || (addressNumber > 1023)) {  // check address range
        Serial.println("Memory address out of range");
        }
    else if ((valueNumber < 0) || (valueNumber > 255)) {  // check value range
        Serial.println("Value out of range");
        }

    else if (command == "write" || command == "w") {
        Serial.print("Writing ");
        Serial.print(valueNumber);
        Serial.print(" on EEEROM memory address: ");
        Serial.println(addressNumber);
        EEPROM.write(addressNumber, valueNumber);
        } 
    else if (command == "read" || command == "read") {
        Serial.print("Value on EERROM address ");
        Serial.print(addressNumber);
        Serial.print(" is = ");
        Serial.println(EEPROM.read(addressNumber), DEC);
        } 
    else {
        Serial.println("Command not recognized");
        }
   }
}
